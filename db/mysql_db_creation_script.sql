-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema restaurant
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema restaurant
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `restaurant` DEFAULT CHARACTER SET utf8 ;
USE `restaurant` ;

-- -----------------------------------------------------
-- Table `restaurant`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurant`.`categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name_en` VARCHAR(40) NOT NULL,
  `name_ua` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name_en` ASC) VISIBLE,
  UNIQUE INDEX `name_ua_UNIQUE` (`name_ua` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `restaurant`.`dish_states`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurant`.`dish_states` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `restaurant`.`dishes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurant`.`dishes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name_en` VARCHAR(40) NOT NULL,
  `name_ua` VARCHAR(40) NOT NULL,
  `price` DECIMAL(7,2) NOT NULL,
  `state_id` INT NOT NULL,
  `category_id` INT NOT NULL,
  PRIMARY KEY (`id`, `state_id`, `category_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name_en` ASC) VISIBLE,
  UNIQUE INDEX `name_ua_UNIQUE` (`name_ua` ASC) VISIBLE,
  INDEX `fk_dishes_status1_idx` (`state_id` ASC) VISIBLE,
  INDEX `fk_dishes_categories1_idx` (`category_id` ASC) VISIBLE,
  CONSTRAINT `fk_dishes_status1`
    FOREIGN KEY (`state_id`)
    REFERENCES `restaurant`.`dish_states` (`id`),
  CONSTRAINT `fk_dishes_categories1`
    FOREIGN KEY (`category_id`)
    REFERENCES `restaurant`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `restaurant`.`order_states`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurant`.`order_states` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `restaurant`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurant`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(254) NOT NULL,
  `password` VARCHAR(60) NOT NULL,
  `name` VARCHAR(40) NOT NULL,
  `surname` VARCHAR(40) NOT NULL,
  `delivery_address` VARCHAR(100) NULL DEFAULT NULL,
  `contact_phone` VARCHAR(15) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `restaurant`.`managers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurant`.`managers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(30) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `login`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `restaurant`.`orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurant`.`orders` (
  `id` INT NOT NULL,
  `state_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `total_price` DECIMAL(12,2) NOT NULL,
  `paid` TINYINT(1) NOT NULL,
  `created` DATETIME NOT NULL,
  `delivery_address` VARCHAR(100) NOT NULL,
  `contact_phone` VARCHAR(15) NOT NULL,
  `managed_by_id` INT NULL,
  PRIMARY KEY (`id`, `state_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_orders_states1_idx` (`state_id` ASC) VISIBLE,
  INDEX `fk_orders_users1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_orders_managers1_idx` (`managed_by_id` ASC) VISIBLE,
  CONSTRAINT `fk_orders_states1`
    FOREIGN KEY (`state_id`)
    REFERENCES `restaurant`.`order_states` (`id`),
  CONSTRAINT `fk_orders_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `restaurant`.`users` (`id`),
  CONSTRAINT `fk_orders_managers1`
    FOREIGN KEY (`managed_by_id`)
    REFERENCES `restaurant`.`managers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `restaurant`.`orders_has_dishes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `restaurant`.`orders_has_dishes` (
  `orders_id` INT NOT NULL,
  `dishes_id` INT NOT NULL,
  `quantity` DECIMAL(7,3) NOT NULL,
  PRIMARY KEY (`orders_id`, `dishes_id`),
  INDEX `fk_orders_has_dish_dish1_idx` (`dishes_id` ASC) VISIBLE,
  INDEX `fk_orders_has_dish_orders1_idx` (`orders_id` ASC) VISIBLE,
  CONSTRAINT `fk_orders_has_dish_dish1`
    FOREIGN KEY (`dishes_id`)
    REFERENCES `restaurant`.`dishes` (`id`),
  CONSTRAINT `fk_orders_has_dish_orders1`
    FOREIGN KEY (`orders_id`)
    REFERENCES `restaurant`.`orders` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
