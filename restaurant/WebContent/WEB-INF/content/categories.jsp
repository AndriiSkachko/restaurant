<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="resources"/>

<!DOCTYPE html>
<html lang="${sessionScope.lang}">

<c:set var="title" value="categories" />
<%@ include file="/WEB-INF/jspf/head.jspf"  %>
<body>
<%@ include file="/WEB-INF/jspf/top_menu.jspf" %>

<form id="categories" action="">

	<table id="list_categories_table">
		<thead>
			<tr>
				<td><fmt:message key="category.id" /></td>
				<td><fmt:message key="category.name" /></td>
				<td></td>
			</tr>
		</thead>

		<c:forEach var="category" items="${categories}">
			<tr>
				<td>${category.id}</td>
				<td>${category.name}</td>
				<td><a href="menu?id=${category.id}">
				<fmt:message key="categories.open" />
				</a></td>
			</tr>
		</c:forEach>
	</table>

</form>
</body>
</html>

