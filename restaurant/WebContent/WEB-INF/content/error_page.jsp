<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="resources"/>

<!DOCTYPE html>
<html lang="${sessionScope.lang}">

<c:set var="title" value="error" />

<%@ include file="/WEB-INF/jspf/head.jspf"  %>

<body>
<%@ include file="/WEB-INF/jspf/top_menu.jspf" %>
<body>
<fmt:message key="common.error" />
<br>
<fmt:message key="${message}" />
<a href="http://localhost:8080/FPRestaurant/"><fmt:message key="top_menu.main" /></a>
</body>
</html>