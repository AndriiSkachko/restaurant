<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="resources"/>

<!DOCTYPE html>
<html lang="${sessionScope.lang}">

<c:set var="title" value="login" />
<%@ include file="/WEB-INF/jspf/head.jspf"  %>
<body>
<%@ include file="/WEB-INF/jspf/top_menu.jspf" %>

<body>
    <div style="text-align: center">
        <form action="login" method="post">
            <label for="email">
            <fmt:message key="login.email" />
            </label>
            <input name="email" size="30" />
            <br><br>
            <label for="password">
            <fmt:message key="login.password" />
            </label>
            <input type="password" name="password" size="30" />
            <br>${message}
            <br><br>           
            <button type="submit">
            <fmt:message key="common.login" />
            </button>
        </form>
    </div>
</body>
</html>