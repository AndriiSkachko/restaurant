<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="resources"/>

<!DOCTYPE html>
<html lang="${sessionScope.lang}">

<c:set var="title" value="logout" />

<%@ include file="/WEB-INF/jspf/head.jspf"  %>

<body>
<%@ include file="/WEB-INF/jspf/top_menu.jspf" %>
<b>${user.name} ${user.surname}</b>
<br>

<form action="" method="post">
  <button type="submit">
  <fmt:message key="top_menu.logout" />
  </button>
</form>
</body>
</html>