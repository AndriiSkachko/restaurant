<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="resources"/>

<!DOCTYPE html>
<html lang="${sessionScope.lang}">

<c:set var="title" value="menu" />
<%@ include file="/WEB-INF/jspf/head.jspf"  %>
<body>
<%@ include file="/WEB-INF/jspf/top_menu.jspf" %>

<form id="menu" action="">

	<table id="list_menu">
		<thead>
			<tr>
				<td><fmt:message key="menu.id" /></td>
				<td><fmt:message key="menu.name" /></td>
				<td><fmt:message key="menu.price" /></td>
				<td></td>
			</tr>
		</thead>

		<c:forEach var="dish" items="${menuItems}">
			<tr>
				<td>${dish.id}</td>
				<td>${dish.name}</td>
				<td>${dish.price}</td>
				<td>
					<a href="cart?&action=buy&id=${dish.id }">
					<fmt:message key="menu.buy" />
					</a>
				</td>
			</tr>
		</c:forEach>
	</table>

</form>

</body>
</html>
