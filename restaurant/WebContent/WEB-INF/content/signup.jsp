<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="resources"/>

<!DOCTYPE html>
<html>
<c:set var="title" value="signup" />
<%@ include file="/WEB-INF/jspf/head.jspf"  %>
<body>
<%@ include file="/WEB-INF/jspf/top_menu.jspf" %>

	<h1><fmt:message key="signup.register" /></h1>
	<p><fmt:message key="signup.fillin" /></p>
	<form id="signup_form" action="signup" method="post">
		<table>
			<tr>
				<td><fmt:message key="signup.email" /></td>
				<td><input type="text" name="email" /></td>
			</tr>
			<tr>
				<td><fmt:message key="signup.password" /></td>
				<td><input type="password" name="password" /></td>
			</tr>
			<tr>
				<td><fmt:message key="signup.name" /></td>
				<td><input type="text" name="name" /></td>
			</tr>
			<tr>
				<td><fmt:message key="signup.surname" /></td>
				<td><input type="text" name="surname" /></td>
			</tr>
			<tr>
				<td><fmt:message key="signup.address" /></td>
				<td><input type="text" name="delivery_address" /></td>
			</tr>
			<tr>
				<td><fmt:message key="signup.phonenum" /></td>
				<td><input type="text" name="contact_phone" /></td>
			</tr>
		</table>
		<button type="submit">Register</button>
	</form>

	<div class="container signin">
		<p>
			<fmt:message key="signup.haveaccount" />
			 <a href="http://localhost:8080/Restaurant/login.jsp"><fmt:message key="signup.login" /></a>.
		</p>
	</div>

</body>
</html>