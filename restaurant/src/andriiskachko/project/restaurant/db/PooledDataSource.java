package andriiskachko.project.restaurant.db;


import java.sql.Connection;
import java.sql.SQLException;


import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;

/**
 * DBCP2 
 * @author Andrii
 *
 */

public class PooledDataSource {
	private static BasicDataSource basicDS = new BasicDataSource();
	static {
		try {
			basicDS.setDriverClassName(ConParam.DRIVER_CLASS); // loads the jdbc driver
			basicDS.setUrl(ConParam.DB_CONNECTION_URL);
			basicDS.setUsername(ConParam.DB_USER);
			basicDS.setPassword(ConParam.DB_PWD);
			// Parameters for connection pooling
			basicDS.setInitialSize(10);
			basicDS.setMaxTotal(10);
			basicDS.setMaxOpenPreparedStatements(100);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() throws SQLException {
		Connection con = null;
		con = basicDS.getConnection();
		con.setAutoCommit(false);
		return con;
	}

	public static DataSource getDataSource() {
	  return basicDS;
  }
	
	public static void commitAndClose(Connection con) {
		try {
			con.commit();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void rollbackAndClose(Connection con) {
		try {
			con.rollback();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}