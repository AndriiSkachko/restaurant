package andriiskachko.project.restaurant.model.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import andriiskachko.project.restaurant.db.PooledDataSource;
import andriiskachko.project.restaurant.model.entity.Category;


 
public class CategoryDAO {
    
	private static final String SQL__FIND_ALL_CATEGORIES = "SELECT * FROM categories";
	
	public List<Category> list(String lang) {
		List<Category> listCategory = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = PooledDataSource.getConnection();
			pstmt = con.prepareStatement(SQL__FIND_ALL_CATEGORIES);
			rs = pstmt.executeQuery();
			CategoryMapper mapper = new CategoryMapper();
			while (rs.next())
                listCategory.add(mapper.mapRow(rs,lang));
		rs.close();
        pstmt.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
			PooledDataSource.rollbackAndClose(con);
		} finally {
			PooledDataSource.commitAndClose(con);
		}

		return listCategory;
	}
	
	private static class CategoryMapper {

        public Category mapRow(ResultSet rs,String lang) {
            try {
               Category category = new Category();
                category.setId(rs.getLong(1));
                category.setName(rs.getString("name_"+lang));
                return category;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
	
}