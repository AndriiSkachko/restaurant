package andriiskachko.project.restaurant.model.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import andriiskachko.project.restaurant.db.PooledDataSource;
import andriiskachko.project.restaurant.model.entity.Dish;


public class MenuDAO {
	private static final String SQL__FIND_ALL_DISHES =
            "SELECT * FROM dishes WHERE category_id = ?";


    /**
     * Returns all menu items.
     *
     * @return List of menu item entities.
     */
	public List<Dish> findMenuItems(int categoryId,String lang) {
		List<Dish> menuItemsList = new ArrayList<Dish>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = PooledDataSource.getConnection();
			MenuItemMapper mapper = new MenuItemMapper();
			pstmt = con.prepareStatement(SQL__FIND_ALL_DISHES);
			pstmt.setInt(1, categoryId);
			rs = pstmt.executeQuery();
			while (rs.next())
				menuItemsList.add(mapper.mapRow(rs,lang));
		} catch (SQLException ex) {
			PooledDataSource.rollbackAndClose(con);
			ex.printStackTrace();

		} finally {
			PooledDataSource.commitAndClose(con);

		}
		return menuItemsList;
	}


    /**
     * Extracts a menu item from the result set row.
     */
    private static class MenuItemMapper {

        public Dish mapRow(ResultSet rs,String lang) {
            try {
               Dish menuItem = new Dish();
                menuItem.setId(rs.getLong(1));
                menuItem.setName(rs.getString("name_"+lang));
                menuItem.setPrice(rs.getDouble(4));
                menuItem.setCategory(rs.getLong(6));
                return menuItem;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}

