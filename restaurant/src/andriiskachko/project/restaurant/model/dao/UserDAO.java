package andriiskachko.project.restaurant.model.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import andriiskachko.project.restaurant.db.PooledDataSource;
import andriiskachko.project.restaurant.model.entity.User;

public class UserDAO {

	/*
	 * private static final String SQL__FIND_USER_BY_EMAIL =
	 * "SELECT * FROM users WHERE EMAIL=?";
	 */
	    private static final String SQL__ADD_USER =
	            "INSERT INTO users (email, password, name, surname, delivery_address, contact_phone) " + 
	            "VALUES (?,?,?,?,?,?);";

	    private static final String SQL_GET_USER =
	            "SELECT * FROM users WHERE email = ? and password = ?";
	
	public User findUser(String email, String password)  {
        User user = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
		try {
			con = PooledDataSource.getConnection();
			pstmt = con.prepareStatement(SQL_GET_USER);
			UserMapper mapper = new UserMapper();
			pstmt.setString(1, email);
			pstmt.setString(2, password);
			rs = pstmt.executeQuery();
            if (rs.next())
                user = mapper.mapRow(rs);
		rs.close();
        pstmt.close();
    } catch (SQLException ex) {
    	PooledDataSource.rollbackAndClose(con);
        ex.printStackTrace();
    } finally {
        PooledDataSource.commitAndClose(con);
    }
    return user;
	}
	
	public void addUser(String email, String password, String name, String surname, String address, String phone) {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = PooledDataSource.getConnection();
			String sql = SQL__ADD_USER;
			statement = con.prepareStatement(sql);
			statement.setString(1, email);
			statement.setString(2, password);
			statement.setString(3, name);
			statement.setString(4, surname);
			statement.setString(5, address);
			statement.setString(6, phone);
			statement.executeUpdate();

			statement.close();
		} catch (SQLException ex) {
			PooledDataSource.rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			PooledDataSource.commitAndClose(con);
		}
	}

	  
	  private static class UserMapper {

	        public User mapRow(ResultSet rs) {
	            try {
	                User user = new User();
	                user.setId(rs.getLong(1));
	                user.setEmail(rs.getString(2));
	                user.setPassword(rs.getString(3));
	                user.setName(rs.getString(4));
	                user.setSurname(rs.getString(5));
	                user.setDeliveryAddress(rs.getString(6));
	                user.setContactPhone(rs.getString(7));
	                return user;
	            } catch (SQLException e) {
	                throw new IllegalStateException(e);
	            }
	        }
	    }
}
