package andriiskachko.project.restaurant.model.entity;


public class Category extends Entity{
	

	private static final long serialVersionUID = -7183556218398633473L;
	
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
