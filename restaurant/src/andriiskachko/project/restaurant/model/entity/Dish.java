package andriiskachko.project.restaurant.model.entity;


public class Dish extends Entity {


	private static final long serialVersionUID = 5245619748816163906L;

	private String name;
	
	private Double price;
	
	private Integer stateId ;
	
	private Long category;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	
	public Long getCategory() {
		return category;
	}

	public void setCategory(Long category) {
		this.category = category;
	}
}