package andriiskachko.project.restaurant.model.entity;


public class DishState extends Entity {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 702916823120823504L;
	
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
