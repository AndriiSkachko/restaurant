package andriiskachko.project.restaurant.model.entity;


import java.io.Serializable;


/**
 * Stub for entities with id field
 * 
 * @author Andrii
 *
 */
public abstract class Entity implements Serializable {

	private static final long serialVersionUID = -6488761499704056325L;
	
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
