package andriiskachko.project.restaurant.model.entity;


public class OrderState extends Entity {

	
	private static final long serialVersionUID = -7073225243250807136L;
	
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
