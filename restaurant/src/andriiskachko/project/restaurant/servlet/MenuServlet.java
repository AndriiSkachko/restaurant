package andriiskachko.project.restaurant.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import andriiskachko.project.restaurant.model.dao.MenuDAO;
import andriiskachko.project.restaurant.model.entity.Dish;


/**
 * Servlet implementation class MenuServlet
 */
@WebServlet("/menu")
public class MenuServlet extends HttpServlet {

		private static final long serialVersionUID = 1L;

		public MenuServlet() {
			super();
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
	        HttpServletRequest req = (HttpServletRequest) request;
	        String lang =(String) req.getSession().getAttribute("lang");
			int id = Integer.parseInt(request.getParameter("id"));
			
			List<Dish> menuItems = new MenuDAO().findMenuItems(id,lang);
			

			// put menu items list to the request
			request.setAttribute("menuItems", menuItems);		
			request.getRequestDispatcher("WEB-INF/content/menu.jsp").forward(request, response);
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
		}

	}

