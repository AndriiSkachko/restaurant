package andriiskachko.project.restaurant.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import andriiskachko.project.restaurant.model.dao.UserDAO;


/**
 * Servlet implementation class RegistrationServlet
 */
@WebServlet("/signup")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String destPage = null;
		if ((request.getSession().getAttribute("user")) == null) {
			destPage = "WEB-INF/content/signup.jsp";
		} else {
			destPage = "WEB-INF/content/logout.jsp";
		}
		response.sendRedirect(destPage);
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String address = request.getParameter("delivery_address");
		String contact = request.getParameter("contact_phone");

		UserDAO userDao = new UserDAO();

		userDao.addUser(email, password, name, surname, address, contact);

		response.sendRedirect("WEB-INF/content/success.jsp");
	}
}
 

