package andriiskachko.project.restaurant.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import andriiskachko.project.restaurant.model.dao.UserDAO;
import andriiskachko.project.restaurant.model.entity.User;

@WebServlet("/login")
public class UserLoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String destPage = null;
		if ((request.getSession().getAttribute("user")) == null) {
			destPage = "WEB-INF/content/login.jsp";
		} else {
			destPage = "WEB-INF/content/logout.jsp";
		}
		request.getRequestDispatcher(destPage).forward(request, response);
	}
	

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");

		UserDAO userDao = new UserDAO();

		User user = userDao.findUser(email, password);
		String destPage = "error";

		if (user != null) {
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			destPage = "success";
			response.sendRedirect(destPage);
		} else {
			String message = "Invalid email/password";
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = request.getRequestDispatcher(destPage);
			dispatcher.forward(request, response);
		}

    }
}
