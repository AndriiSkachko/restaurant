package andriiskachko.project.restaurant.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/logout")
public class UserLogoutServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public UserLogoutServlet() {
        super();
    }
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String destPage = null;
		if ((request.getSession().getAttribute("user")) == null) {
			destPage = "/WEB-INF/content/login.jsp";
		} else {
			destPage = "/WEB-INF/content/logout.jsp";
		}
		request.getRequestDispatcher(destPage).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String destPage = "success";
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.removeAttribute("user");
		}
		response.sendRedirect(destPage);
	}
}